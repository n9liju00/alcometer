import React,{ useState } from 'react';
import { StyleSheet, Text, TextInput, View, Button } from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import RadioForm from 'react-native-simple-radio-button';


export default function App() {
  const [weight, setWeight] = useState(null)
  const [open1, setOpen1] = useState(false);
  const [value1, setValue1] = useState(null);
  const [items1, setItems1] = useState([
    { label: '1 Bottle', value: '1' },
    { label: '2 Bottles', value: '2' },
    { label: '3 Bottles', value: '3' },
    { label: '4 Bottles', value: '4' },
    { label: '5 Bottles', value: '5' },
  ]);
  const [open2, setOpen2] = useState(false);
  const [value2, setValue2] = useState(null);
  const [items2, setItems2] = useState([
    { label: '1 hour', value: '1' },
    { label: '2 hours', value: '2' },
    { label: '3 hours', value: '3' },
    { label: '4 hours', value: '4' },
    { label: '5 hours', value: '5' },
    { label: '6 hours', value: '6' },
    { label: '7 hours', value: '7' },
    { label: '8 hours', value: '8' },
    { label: '9 hours', value: '9' },
    { label: '10 hours', value: '10' },
    { label: '11 hours', value: '11' },
    { label: '12 hours', value: '12' },
  ]);

  const genders = [
    {label: 'Male', value: 0 },
    {label: 'Female', value: 1 }
  ];
  const [gender, setGender] = useState(0);
  const [promilles, setPromilles] = useState(null);
  

  


  function calculate() {
    var formula = 0;
    if (gender === 0) {
      formula = 0.7
    }
    if (gender === 1) {
      formula = 0.6
    }
    
    
    const result = (((value1 * 0.33) * 8 * 4.5) - ((weight / 10) * value2)) / (weight * formula);
    
    if (result < 0) {
      setPromilles(0)
    } else {
      setPromilles(result.toFixed(2))
    }
    



  }
  

  return (
    <>
      <View style={styles.weight}>
        <Text>Weight</Text><TextInput
          value={weight}
          onChangeText={text => setWeight(text)}
          placeholder="Set weight"/>
      </View>
      <View style={styles.drop}>
        <Text>Bottles</Text>
        <DropDownPicker
        open={open1}
        value={value1}
        items={items1}
        setOpen={setOpen1}
        setValue={setValue1}
        setItems={setItems1}
        />
      </View>
      <View style={styles.drop2}>
        <Text>Time</Text>
        <DropDownPicker
        open={open2}
        value={value2}
        items={items2}
        setOpen={setOpen2}
        setValue={setValue2}
          setItems={setItems2}
          dropDownDirection="TOP"
        />
      </View>
      <View>
        <Text>Gender</Text>
        <RadioForm
          radio_props={genders}
          initial={0}
          onPress={(value) => {setGender(value)}}
        />
      </View>
      <View>
        <Text>Promilles</Text>
        <Text>{promilles}</Text>
      </View>
      <Button onPress={calculate} title="Calculate"></Button>
    </>
  );
}

const styles = StyleSheet.create({
  weight: {
    marginTop: 40,
  },
  drop: {
    marginBottom: 220,
    
  },
  drop2: {

  }
});
